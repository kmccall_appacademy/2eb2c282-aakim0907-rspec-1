def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  return 0 if arr.length == 0
  arr.reduce(:+)
end

def multiply(arr)
  arr.reduce(:*)
end

def power(num1, num2)
  num1 ** num2
end

def factorial(num)
  return 0 if num == 0
  (1..num).to_a.reduce(:*)
end
