def echo(sth)
  sth
end

def shout(sth)
  sth.upcase
end

def repeat(sth, times=2)
  words = [sth]*times
  words.join(" ")
end

def start_of_word(sth, many)
  sth[0..many-1]
end

def first_word(sentence)
  sentence.split(" ")[0]
end

def titleize(sentence)
  little_words = ['and', 'over', 'the']
  titleized_words = []
  sentence.split(" ").each_with_index do |wrd, idx|
    if idx == 0
      titleized_words << wrd.capitalize
    elsif little_words.include?(wrd)
      titleized_words << wrd
    else
      titleized_words << wrd.capitalize
    end
  end

  titleized_words.join(" ")    
end
