def pig_latinize(word)
  translated_word = ""
  starting_consonant = ""
  vowels = ["a", "e", "i", "o"]

  word.chars.each_with_index do |ch, idx|
    if vowels.include?(ch)
      translated_word += word[idx..-1]
      break
    else
      starting_consonant += ch
    end
  end

  translated_word += starting_consonant + "ay"
end

def translate(sentence)
  translated_words = []

  sentence.split(" ").each do |ele|
    translated_words << pig_latinize(ele)
  end

  translated_words.join(" ")
end
